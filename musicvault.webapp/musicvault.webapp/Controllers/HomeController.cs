﻿using Microsoft.AspNetCore.Mvc;
using musicvault.webapp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace musicvault.webapp.Controllers
{
    public class HomeController : Controller
    {
        #region Fields

        private MusicVaultContext _Context;

        #endregion

        #region Ctor

        public HomeController(MusicVaultContext context)
        {
            _Context = context;
        }

        #endregion

        #region Methods

        public IActionResult Index()
        {
            ViewBag.NumberOfArtists = _Context.Artists.Count();
            ViewBag.NumberOfAlbums = _Context.Albums.Count();
            return View();
        }

        #endregion
    }
}
