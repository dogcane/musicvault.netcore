﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace musicvault.webapp.Models
{
    public class Album
    {
        [Key]
        public virtual int Id { get; set; }
        [Required]
        public virtual int ArtistId { get; set; }        
        public virtual Artist Author { get; set; }
        public virtual List<Track> Tracks { get; set; }
        public virtual byte[] Cover { get; set; }
    }
}
