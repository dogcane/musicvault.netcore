﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace musicvault.webapp.Models
{
    public class Group : Artist
    {
        public virtual List<Artist> Members { get; set; }
    }
}
