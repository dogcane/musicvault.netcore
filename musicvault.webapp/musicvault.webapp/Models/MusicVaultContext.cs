﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace musicvault.webapp.Models
{
    public class MusicVaultContext : DbContext
    {
        public MusicVaultContext(DbContextOptions<MusicVaultContext> options) : base(options)
        {

        }

        public DbSet<Album> Albums { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Group> Groups { get; set; }
    }
}
