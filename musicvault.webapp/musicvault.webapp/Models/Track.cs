﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace musicvault.webapp.Models
{
    public class Track
    {
        [Key]
        public virtual int Id { get; set; }
        [Required]
        public virtual int Number { get; set; }
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual byte Minutes { get; set; }
        [Required]
        public virtual byte Seconds { get; set; }
    }
}
